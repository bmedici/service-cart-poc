require "grape-entity"

module ServiceCart
  module API
    module V1
      module Entities

        class Ticket < Base
          expose :cart_id, :format_with => :to_string
          expose :name
          expose :type
          expose :device
          expose :sitename
          expose :promotion_id
          # expose :cart, using: Entities::Cart
        end

      end
    end
  end
end