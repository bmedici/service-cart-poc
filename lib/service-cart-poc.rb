# Global libs
require "rubygems"
require 'bmc-daemon-lib'
require "json"
require "haml"
require "mongoid"
require "grape"
require "grape-entity"
require 'grape-swagger'



# Project's libs
require_relative "service-cart-poc/constants"

# Helpers
require_relative "service-cart-poc/helpers/common"
require_relative "service-cart-poc/helpers/api"
require_relative "service-cart-poc/helpers/response"

# Models
require_relative "service-cart-poc/models/common"
require_relative "service-cart-poc/models/ticket"
require_relative "service-cart-poc/models/event"
require_relative "service-cart-poc/models/cart"

# API
require_relative "service-cart-poc/api/defaults"
require_relative "service-cart-poc/api/tools"
require_relative "service-cart-poc/api/v1/base"
require_relative "service-cart-poc/api/root"
