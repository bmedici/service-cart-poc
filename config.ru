# FIXME: Newrelic agent bug: we have to define the logfile early
Conf.prepare_newrelic

# Load code
Conf.log :rackup, "load project code"

load_path_libs = File.expand_path "lib", File.dirname(__FILE__)
$LOAD_PATH.unshift(load_path_libs) unless $LOAD_PATH.include?(load_path_libs)
require "service-cart-poc"

# Serve static assets
use Rack::Static, root: "#{Conf.app_libs}/static/", urls: [
  "/swagger/",
  MOUNT_SWAGGER_UI,
  ]

# Env-specific configuration
case Conf.app_env
  when "development"
    # Rack reloader
    use Rack::Reloader, 1

  when "production"
end

# Connect to database
Conf.log :rackup, "database [#{Conf.at(:mongo, :base)}] on [#{Conf.at(:mongo, :host)}]"
Mongoid.load_configuration(clients: {
  default: {
    hosts: [Conf.at(:mongo, :host)],
    database: Conf.at(:mongo, :base),
  }
})

# Mongoid.raise_not_found_error = true

# Launch the API
Conf.log :rackup, "start API::Root"
run ServiceCart::API::Root

# run Rack::Cascade.new [Example, Main]