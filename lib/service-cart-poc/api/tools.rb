module ServiceCart
  module API
    class Tools < Grape::API
      include BmcDaemonLib


      ### HELPERS
      helpers do
        def log_context
          {caller: "API::Tools"}
        end
      end


      ### ENDPOINT
      get tags: ['tools'] do
        routes = []
        API::Root.routes.each do |route|
          routes << "#{route.options[:method]} #{route.pattern.path}"
        end

        # Build response
        return  {
          name: Conf.app_name,
          version: Conf.app_ver,
          started: Conf.app_started,
          hostname: `hostname`.to_s.chomp,
          routes: routes,
          content_types: Grape::ContentTypes::CONTENT_TYPES,
          }
      end

      ### ENDPOINT
      get :ping, tags: ['tools'] do
        {
          ok: true
        }
      end


      ### ENDPOINT
      get :feed, tags: ['tools'] do
        # First save the Cart to avoid Mongoid::Errors::UnsavedDocument

        Cart.delete_all
        Ticket.delete_all
        Event.delete_all

        Cart.create name: "Cart 1", _id: "cart1" do |cart|
          cart.save 
          cart.tickets.create(name: "Ticket Draw 1A", type: "draw", _id: "ticket1a")
          t1b = cart.tickets.create(name: "Ticket Draw 1B", type: "draw", _id: "ticket1b")
          cart.events.create(type: "cart-creation")
          cart.events.create(type: "ticket-add", ticket: t1b)
          cart.events.create(type: "cart-scan")
          cart.events.create(type: "ticket-pay", ticket: t1b)
        end

        Cart.create name: "Cart 2", _id: "cart2" do |cart|
          cart.save 
          cart.tickets.create(name: "Ticket Sport 2C", type: "sport", _id: "ticket32c")
        end

        Cart.create name: "Cart 3", _id: "cart3" do |cart|
          cart.save
          cart.tickets.create(name: "Ticket Sport 3D", type: "draw", _id: "ticket3d")
          cart.tickets.create(name: "Ticket Sport 3E", type: "sport", _id: "ticket3e")
        end

        Cart.all
      end      

    end
  end
end
