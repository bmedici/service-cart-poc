require "zlib"

module ServiceCart
  module API
    module V1
      module Entities

        class Cart < Base

          expose :name
          expose :status
          expose :player_uid

          expose :tickets, using: Entities::Ticket, if: :include_tickets
          expose :events, using: Entities::Event, if: :include_events

          def to_png
            # Build code payload
            data = {
              id: object.id,
              name: object.id,
              tickets_count: object.tickets.count,
              created_at: object.created_at,
              generated_at: Time.now,
              }
            # return payload
            puts "to_png: #{data.inspect}"

            # Build payload
            payload = Zlib::Deflate.deflate(data.to_json)
            payload = data.to_json

            # Build barcode
            barcode = Barby::DataMatrix.new(payload)
            outputter = Barby::PngOutputter.new(barcode)
            outputter.xdim = 8
            # outputter.width = 800
            # outputter.height = 800
            outputter.to_png
          end

        end
      end
    end
  end
end