class Ticket
  include MongoidDocumentCommon

  belongs_to :cart
  has_many :events

  field :name, type: String
  field :type, type: String
  field :device, type: String
  field :sitename, type: String
  field :promotion_id, type: String
end