module MongoidDocumentCommon
  extend ActiveSupport::Concern

  included do
    include Mongoid::Document
  
    before_save :set_updated_at

    field :created_at, type: DateTime, default: ->{ Time.now }
    field :updated_at, type: DateTime, default: ->{ Time.now }
  end

  def set_updated_at
    self.updated_at = Time.now
  end

end
