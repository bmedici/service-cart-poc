# Docker headers
FROM ruby:2.4.2-alpine3.6
MAINTAINER Bruno MEDICI <opensource@bmconseil.com>


# Environment
ENV LANG=C.UTF-8
ENV INSTALL_PATH /app/


# Install system packages
RUN apk add --update ruby-dev build-base
RUN apk add --update git sqlite-dev libstdc++


# Prepare bundler
RUN gem install bundler --no-rdoc --no-ri


# Change to INSTALL_PATH and install base packages
RUN mkdir -p        $INSTALL_PATH
WORKDIR             $INSTALL_PATH
ADD Gemfile         $INSTALL_PATH
ADD *.gemspec 		$INSTALL_PATH
RUN bundle install --system --without="development test" -j4


# Cleanup image
RUN apk del ruby-dev build-base


# Install app code
ADD . $INSTALL_PATH


# App run information
EXPOSE 2000
CMD ["bin/service-cart", "-c", "/etc/service-cart.yml", "-f", "start"]
