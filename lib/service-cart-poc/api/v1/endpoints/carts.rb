module ServiceCart
  module API
    module V1
      class Carts < Grape::API
        include Defaults


        ### HELPERS
        helpers do
          def log_context
            {caller: "API::V1::Carts"}
          end
        end


        ## FORMATTERS
        content_type :png, "image/png"
        formatter :png, ->(object, env) { object.to_png }
             

        ### ENDPOINTS FOR CART
        desc "Read with id", is_array: false, http_codes: API_HTTP_ELEMENT
        params do
          requires :id, type: String, desc: "id of the element to read"
        end
        get "carts/:id", requirements: { id: API_ID_SPEC } do
          found = Cart.find(params[:id])
          respond_with found, Entities::Cart
        end

        desc "List all", is_array: true, tags: ['carts'], http_codes: API_HTTP_COLLECTION
        get "carts/" do
          found = Cart.all
          respond_with found, Entities::Cart
        end

        desc "Create a new element"
        params do
          requires :name,
            type: String,
            desc: "Name of the cart",
            allow_blank: false
        end
        post "carts/" do
          # Prepare elements
          element = Cart.create name: params[:name]

          # Prepare response
          status 201
          present element, with: Entities::Cart
        end

      end
    end
  end
end