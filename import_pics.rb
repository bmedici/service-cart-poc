require "sequel"
DB= Sequel.connect('sqlite://service-cart.db')

Dir['pics/*'].each do |x|
  data = IO.read x

  matches = x.match /\/(.+)\.pic/
  name = matches[1]
  next unless name

  DB[:pics] << {
    name: name,
    size: data.bytesize,
    image: IO.read(x)
    }
end

#DB[:pics] << {name: "name1", image: "pppp"}



