DOCKER_REPO = "bmconseil/service-cart"

desc "Build docker image from local code"


task :dockerize => [] do
  puts
  puts "* build"
  sh "docker build . -t '#{DOCKER_REPO}:latest'"

  puts
  puts "* push to [#{DOCKER_REPO}]"
  sh "docker push #{DOCKER_REPO}:latest"

  puts
end
