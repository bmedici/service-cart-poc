require_relative "entities/base"
require_relative "entities/ticket"
require_relative "entities/event"
require_relative "entities/cart"

require_relative "endpoints/tickets"
require_relative "endpoints/events"
require_relative "endpoints/carts"

# (Dir["./endpoints/*.rb"] + Dir["./entities/*.rb"]).each do |file|
#   require file
# end



module ServiceCart
  module API
    module V1

      class Base < Grape::API
        version 'v1', using: :path, cascade: true
        mount Carts
        mount Tickets
        mount Events
      end

    end
  end   
end
