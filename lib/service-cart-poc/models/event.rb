class Event
  include MongoidDocumentCommon

  belongs_to :cart
  belongs_to :ticket

  field :type, type: String
  field :user_id, type: String

end