module ServiceCart
  module API
    module V1
      class Tickets < Grape::API
        include Defaults


        ### HELPERS
        helpers do
          def log_context
            {caller: "API::V1::Tickets"}
          end
        end

        ### ENDPOINTS
        desc "Read with id", is_array: false, tags: ['tickets'], http_codes: API_HTTP_ELEMENT
        params do
          requires :id, type: String, desc: "id of the element to read"
        end
        get "tickets/:id", requirements: { id: API_ID_SPEC } do
          found = Ticket.find(params[:id])
          respond_with found, Entities::Ticket
        end

        desc "List all", is_array: true, tags: ['tickets'], http_codes: API_HTTP_COLLECTION
        get :tickets do
          found = Ticket.all
          respond_with found, Entities::Ticket
        end

        desc "Create a new element"
        params do
          requires :name, type: String, desc: "Name of the element", allow_blank: false
          requires :cart_id, type: String, desc: "ID of the cart", allow_blank: false
        end
        post :tickets do
          found = Ticket.create name: params[:name], cart_id: params[:cart_id]
          status 201
          present found, with: Entities::Ticket
        end


        ### ENDPOINTS UNDER CARTS
        desc "Read cart ticket", is_array: false, tags: ['tickets'], http_codes: API_HTTP_ELEMENT
        params do
          requires :id, type: String, desc: "id of the element to read"
        end
        get "carts/:cart_id/tickets/:id", requirements: { cart_id: API_ID_SPEC, id: API_ID_SPEC } do     
          found = Ticket.find_by(cart_id: params[:cart_id], id: params[:id])
          respond_with found, Entities::Ticket
        end
        post "carts/:cart_id/tickets/", requirements: { cart_id: API_ID_SPEC, id: API_ID_SPEC } do     
          found = Ticket.create name: params[:name], cart_id: params[:cart_id]
          status 201
          present found, with: Entities::Ticket
        end

        desc "List cart tickets", is_array: true, tags: ['carts'], http_codes: API_HTTP_COLLECTION
        get "carts/:cart_id/tickets/", requirements: { cart_id: API_ID_SPEC } do
          found = Ticket.where(cart_id: params[:cart_id]).all
          respond_with found, Entities::Ticket
        end     

      end
    end
  end
end