module ServiceCart
  module API
    module V1
      class Events < Grape::API
        include Defaults


        ### HELPERS
        helpers do
          def log_context
            {caller: "API::V1::Events"}
          end
        end


        ### ENDPOINTS
        desc "Read with id", is_array: false, tags: ['events'], http_codes: API_HTTP_ELEMENT
        params do
          requires :id, type: String, desc: "id of the element to read"
        end
        get "events/:id", requirements: { id: API_ID_SPEC } do
          found = Event.find(params[:id])
          respond_with found, Entities::Event
        end

        desc "List all", is_array: true, tags: ['events'], http_codes: API_HTTP_COLLECTION
        get :events do
          found = Event.all
          respond_with found, Entities::Event
        end

        desc "Create a new element"
        params do
          requires :name, type: String, desc: "Name of the element", allow_blank: false
          requires :cart_id, type: String, desc: "ID of the cart", allow_blank: false
        end
        post :events do
          found = Event.create name: params[:name], cart_id: params[:cart_id]
          status 201
          present found, with: Entities::Event
        end


        ### ENDPOINTS UNDER CARTS
        desc "Read cart event", is_array: false, tags: ['events'], http_codes: API_HTTP_ELEMENT
        params do
          requires :id, type: String, desc: "id of the element to read"
        end
        get "carts/:cart_id/events/:id", requirements: { cart_id: API_ID_SPEC, id: API_ID_SPEC } do     
          found = Event.find_by(cart_id: params[:cart_id], id: params[:id])
          respond_with found, Entities::Event
        end

        desc "List cart events", is_array: true, tags: ['carts'], http_codes: API_HTTP_COLLECTION
        get "carts/:cart_id/events/", requirements: { cart_id: API_ID_SPEC } do
          found = Event.where(cart_id: params[:cart_id]).all
          respond_with found, Entities::Event
        end     

      end
    end
  end
end