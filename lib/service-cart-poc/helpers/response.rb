require 'barby'
require 'barby/barcode/data_matrix'
require 'barby/outputter/png_outputter'

module ServiceCart
  module ResponseHelpers

    def respond_with payload, presenter
      # Some debug
      if payload.is_a? Enumerable
        log_debug "respond_with collection of (#{payload.size})"
      elsif payload.nil?
        log_debug "respond_with NIL"
      elsif payload.respond_to? :id
        log_debug "respond_with object id (#{payload.id})"
      else
        log_debug "respond_with object without id"
      end

      # Set presenter
      options = {
        with: presenter
      }

      # Initialize "includes" flags
      params[:include].to_s.split(',').each do |entity|
        log_debug "include: #{entity}"
        options["include_#{entity}".to_sym] = true
      end
      log_debug "present options", options

      # Format output
      status 200
      present payload, options
    end    

  end
end
