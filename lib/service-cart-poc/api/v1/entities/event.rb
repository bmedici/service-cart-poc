require "grape-entity"

module ServiceCart
  module API
    module V1
      module Entities

        class Event < Base
          expose :cart_id, :format_with => :to_string
          expose :ticket_id, :format_with => :to_string
          expose :type
          expose :user_id
        end

      end
    end
  end
end