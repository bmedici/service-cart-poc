# Infos du service (version, désignation, etc)
https://cart.api.bmconseil.com/

# Doc Swagger du service
https://cart.api.bmconseil.com/swagger.html

# Lecture des carts
https://cart.api.bmconseil.com/v1/carts/

# Lecture du cart1 uniquement
https://cart.api.bmconseil.com/v1/carts/cart1

# Création d'un cart de tests
curl -H "Content-Type: application/json" -X POST -D /dev/stdout -d '{"name":"Cart de test"}' "https://cart.api.bmconseil.com/v1/carts/"

# Création d'un ticket sur le cart2
curl -H "Content-Type: application/json" -X POST -D /dev/stdout -d '{"name":"Ticket de test dans le cart2"}' "https://cart.api.bmconseil.com/v1/carts/cart2/tickets/"

# Lecture des tickets de ce cart2
https://cart.api.bmconseil.com/v1/carts/cart2/tickets/

# Lecture de tous les tickets
https://cart.api.bmconseil.com/v1/tickets/

# Lecture des tickets du cart1
https://cart.api.bmconseil.com/v1/carts/cart1/tickets/

# Lecture des évenements du cart1
https://cart.api.bmconseil.com/v1/carts/cart1/events/

# Lecture du cart1 en incluant ses tickets et les évènements sur le cart ou sur ses tickets
https://cart.api.bmconseil.com/v1/carts/cart1?include=tickets,events

# Lecture du cart1 uniquement, en vue Datamatrix
https://cart.api.bmconseil.com/v1/carts/cart1.png

