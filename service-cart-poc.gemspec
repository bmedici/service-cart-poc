# coding: utf-8
Gem::Specification.new do |spec|

  # Project version
  spec.version                    = "0.5.2"

  # Project description
  spec.name                       = "service-cart-poc"
  spec.authors                    = ["Bruno MEDICI"]
  spec.email                      = "opensource@bmconseil.com"
  spec.description                = ""
  spec.summary                    = ""
  spec.homepage                   = "http://github.com/bmedici/service-cart-poc"
  spec.licenses                   = ["MIT"]
  spec.date                       = Time.now.strftime("%Y-%m-%d")

  # List files and executables
  spec.files                      = `git ls-files -z`.
                                      split("\x0").
                                      reject{ |f| f =~ /^dashboard.+\.png/ }
  spec.executables                = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.require_paths              = ["lib"]
  spec.required_ruby_version      = ">= 2.4.2"

  # Development dependencies
  spec.add_development_dependency "bundler", "~> 1.6"
  spec.add_development_dependency "rake"
  spec.add_development_dependency "http"

  # Runtime dependencies
  spec.add_runtime_dependency     "bmc-daemon-lib", "~> 0.12.2"
  spec.add_runtime_dependency     "json", "~> 1"
  spec.add_runtime_dependency     "thin", "~> 1.7"
  spec.add_runtime_dependency     "activesupport", "~> 4.2"

  spec.add_runtime_dependency     "chunky_png"
  spec.add_runtime_dependency     "semacode-ruby19"
  spec.add_runtime_dependency     "barby"
  spec.add_runtime_dependency     "grape"
  spec.add_runtime_dependency     "grape-entity"
  spec.add_runtime_dependency     "grape-swagger"
  spec.add_runtime_dependency     "mongoid"
  spec.add_runtime_dependency     "haml"
end
