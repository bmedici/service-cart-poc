module ServiceCart
  module ApiHelpers

    def logger
      ServiceCart::API::Root.logger
    end

    def set_base_headers
      header 'X-Service', Conf.app_name
      header 'X-Version', Conf.app_ver.to_s
    end

    def declare_css *names
      header 'X-Expect-CSS', names.map(&:to_s).join(', ')
    end

    def declare_js *names
      header 'X-Expect-JS', names.map(&:to_s).join(', ')
    end

    def log_request
      if env.nil?
        puts "HTTP_ENV_IS_NIL: #{env.inspect}"
        return
      end

      request_method = env['REQUEST_METHOD']
      request_path   = env['REQUEST_PATH']
      request_uri    = env['REQUEST_URI']
      log_info       "HTTP #{request_method} #{request_uri}", params
    end

    def get_censored_config
      config              = Conf.to_hash
      config[:users]      = Conf[:users].keys if Conf[:users]
      config[:endpoints]  = Conf[:endpoints].keys if Conf[:endpoints]
      config[:credentials]  = Conf[:credentials].keys if Conf[:credentials]
      config
    end

    def exception_error error, http_code, exception, include_backtrace = false
      # Extract message lines
      lines = exception.message.lines

      # Log error to file
      log_error "[#{error}] [#{http_code}] #{lines.shift}", lines

      # Build error 
      http_msg = {
        error: error,
        http_code: http_code,
        message: exception.class.name
        }

      http_msg[:backtrace] = exception.backtrace if include_backtrace

      # Return error
      error!(http_msg, http_code)
    end

    def render name, values={}
      # Prepare template engine
      template = File.read("#{Conf.app_libs}/views/block/#{name}.haml")
      haml_engine = Haml::Engine.new(template, encoding: Encoding::UTF_8)

    rescue Errno::ENOENT => e
      status 404
      body '404: template not found'
      #return :template_not_found

    rescue StandardError => e
      return e.inspect

    else
      scope_object = eval("self", binding)
      haml_engine.render(scope_object, values)
    end

  end
end
