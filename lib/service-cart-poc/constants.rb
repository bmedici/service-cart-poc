# Configuration defaults
DEFAULT_POOL            = "default"
DEFAULT_SFTP_TIMEOUT    = 600   # 10mn
DEFAULT_PAGE_SIZE       = 50    # 50 lines
DEFAULT_RETRY_AFTER     = 10    # 10s
TARGET_BLANK             = "_blank"


# Constants: logger
LOGGER_FORMAT = {
  # context:  "%#{-LOG_PREFIX_WID.to_i}s %#{-LOG_PREFIX_JID.to_i}s %#{-LOG_PREFIX_ID.to_i}s ",
  # context:  "wid:%-8{wid} jid:%-12{jid} id:%-5{id}",
  context: {
    caller: "%-17s",
    wid:    "%-10s",
    jid:    "%-10s",
    id:     "%-8s",
    }
  }


# API mountpoints
MOUNT_SWAGGER_JSON        = "/swagger.json"
MOUNT_SWAGGER_UI          = "/swagger.html"


API_HTTP_ELEMENT = [
        { code: 200, message: "Here is the element you requested" },
        { code: 404, message: "Element not found" }
        ]

API_HTTP_COLLECTION = [
        { code: 200, message: "Here are the elements you requested" },
        ]

API_ID_SPEC = /[^\/^\.]+/
