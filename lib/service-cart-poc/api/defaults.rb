module ServiceCart
  module API
    module Defaults
      extend ActiveSupport::Concern
      include BmcDaemonLib

      included do

        ## EXCEPTION HANDLERS
        rescue_from Mongo::Error::NoServerAvailable do |exception|
          exception_error :db_server_unavailable, 400, exception
        end
        rescue_from Mongoid::Errors::UnknownAttribute do |exception|
          exception_error :db_unknown_attribute, 400, exception
        end
        rescue_from Mongoid::Errors::DocumentNotFound do |exception|
          exception_error :not_found, 404, exception
        end
        rescue_from Mongoid::Errors do |exception|
          exception_error :db_error, 404, exception
        end
        rescue_from Grape::Exceptions::InvalidMessageBody do |exception|
          exception_error :api_invalid_message_body, 400, exception
        end
        rescue_from :all do |exception|
          puts exception.backtrace.join("\n")
          exception_error :api_error, 500, exception
        end


        ### HELPERS
        helpers do
          def log_context
            {caller: "API"}
          end
        end

      end
    end
  end
end
