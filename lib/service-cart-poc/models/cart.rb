class Cart
  include MongoidDocumentCommon

  has_many :tickets
  has_many :events

  field :name, type: String
  field :status, type: String
  field :active, type: Mongoid::Boolean
  field :player_uid, type: String

end
