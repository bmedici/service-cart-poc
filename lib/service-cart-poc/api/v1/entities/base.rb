require "grape-entity"

module ServiceCart
  module API
    module V1
      module Entities

        class Base < Grape::Entity

          expose :id, :format_with => :to_string
          expose :created_at
          expose :updated_at

          format_with(:to_string) do |foo|
            foo.to_s
          end
        
        end
      end
    end
  end
end