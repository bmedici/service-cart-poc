module ServiceCart
  module API

    class Root < Grape::API
      include BmcDaemonLib::LoggerHelper

      ### COMMON CONFIG AND LOGGER
      do_not_route_head!
      do_not_route_options!
      # version 'v1'
      logger BmcDaemonLib::LoggerPool.instance.get :api


      ### RESPONSE FORMAT 
      format :json
      content_type :txt, 'text/plain'
      default_format :json

     
      ### HELPERS
      helpers BmcDaemonLib::LoggerHelper
      helpers ServiceCart::CommonHelpers
      helpers ServiceCart::ApiHelpers
      helpers ServiceCart::ResponseHelpers
      helpers do
        def log_context
          {caller: "API::Root"}
        end
      end

      ### BEFORE/AFTER
      before do
        log_request
        set_base_headers
      end
    

      ### MOUNTPOINTS
      mount API::Tools
      mount API::V1::Base

      
      ### API DOCUMENTATION
      add_swagger_documentation hide_documentation_path: true,
        api_version: Conf.app_ver,
        doc_version: Conf.app_ver,
        mount_path: MOUNT_SWAGGER_JSON,
        info: {
          title: Conf.app_name,
          version: Conf.app_ver,
          description: "API description for #{Conf.app_name} #{Conf.app_ver}",
          }


    end
  end
end